#include <stdio.h>      // Default System Calls
#include <stdlib.h>     // Needed for OS X
#include <string.h>     // Needed for Strlen
#include <sys/socket.h> // Needed for socket creating and binding
#include <netinet/in.h> // Needed to use struct sockaddr_in
#include <time.h>       // To control the timeout mechanism
#include "server_info.h"
#define EXPR_SIZE   1024
#define BUFLEN      512
#define TRUE        1
#define SERVERLEN   1024

void die(char *s)
{
    perror(s);
    exit(1);
}

int ask_introducer(struct sockaddr_in si_other, int s)
{
    char message[BUFLEN];
    si_other.sin_addr.s_addr = htonl(inet_addr(intro_ip_address));  
    message[0] = 'J';
    int server_number;
    if (sendto(s, message, strlen(message) , 0 , (struct sockaddr *) &si_other, sizeof(si_other))==-1)
    {
	die("sendto()");
    }
    //receive a reply and print it
    //clear the buffer by filling null, it might have previously received data
    //memset(buf,'\0', BUFLEN);
    //try to receive some data, this is a blocking call
    if (recvfrom(s, &server_number, 4, 0, (struct sockaddr *) &si_other, 0) == -1)
    {
	die("recvfrom()");
    }
    printf("recived authorization from introducer joining as Server %d\n", server_number);
    //puts(buf);
    return server_number;
}




int main( void )
{
    int fd;
    if ( (fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket failed");
        return 1;
    }

    struct sockaddr_in serveraddr;
    memset( &serveraddr, 0, sizeof(serveraddr) );
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(8888);              

    ask_introducer(serveraddr, fd);

    close( fd );
}
