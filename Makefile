default: server my_intro

server: server.c
		gcc server.c -o server -lpthread

my_intro: my_intro.c
		gcc  my_intro.c -o my_intro -lpthread

client: client.cpp
	     	g++ -static client.cpp -lboost_regex -L/usr/include/boost/lib -lboost_program_options -lpthread -o client
#grep : grep.cpp
#		g++ -static grep.cpp -lboost_regex -L/usr/include/boost/lib -lboost_program_options -lpthread -o grep
