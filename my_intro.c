/*
    Simple udp server
*/
#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include"server_info.h"
#include<time.h>
#include<pthread.h>

#define BUFLEN 512  //Max length of buffer
#define PORT 8888   //The port on which to listen for incoming data

struct Member_id membership[NUMBER_OF_SERVERS];

void die(char *s)
{
    perror(s);
    exit(1);
}

void *keyboard_thread(void *vargp)
{
    while(1){
      char c = getchar();
      //printf("char:%c",c);
      printf("serverid:%d\n",*((int *)vargp));
      int i;
      if(c == 'l')
      {
		for(i = 0; i < 10; i++)
		{
			printf("Server %d Active flag %d, timestamp %lu\n", membership[i].server_number, membership[i].active, membership[i].timestamp);
		}
      }
    }
    return NULL;
}
void check_for_new(int s)
{
	struct sockaddr_in si_other;

    	int i, slen = sizeof(si_other) , recv_len;
	char buf[BUFLEN];
	fflush(stdout);

        //try to receive some data, this is a blocking call
        if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
        {
            die("recvfrom()");
        }

        //print details of the client/peer and the data received
//        printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
//        printf("Data: %s\n" , buf);
  if (buf[0] == 'J'){
    char * jr_ip_address = inet_ntoa(si_other.sin_addr);
  	for(i = 0; i < NUMBER_OF_SERVERS-1; i++)
  	{
  		if(strcmp(jr_ip_address,ip_address[i]) == 0)
  		{
  			printf("Request to join made by Server %d\n", i+1);
  			char message [255];
  			char* request = "Request to join made by Server";
  			char number[2];
  			sprintf(number, "%d", i+1);
  			strcat(message, request);
  			strcat(message, number);
  			strcat(message, "\n");
  			write_to_log(message);
			int sn = i+1;
        		
        		membership[i+1].timestamp = time(NULL);
			membership[i+1].active = 1;
  			break;
  		}
  	}
  	i = i+1;

          //now reply the client with the same data
          if (sendto(s, &i, 4, 0, (struct sockaddr*) &si_other, slen) == -1)
          {
              die("sendto()");
          }
  }else{
    
	//printf("%c",buf[0]);
    int server_fail = buf[0]-'0';
    membership[server_fail].active = 0;
    //should broadcast to all server now
    // for ()
    for(i = 0; i < 9; i++)
    {
        if(membership[i].active==0) continue;
    	if (inet_aton(ip_address[i] , &si_other.sin_addr) == 0)
    	{
        	fprintf(stderr, "inet_aton() failed\n");
        	exit(1);
    	}
    	if (sendto(s, &membership, sizeof(struct Member_id)*10 , 0 , (struct sockaddr *) &si_other, sizeof(si_other))==-1)
    	{
    		die("sendto()");
    	}
    	printf("Updating Server %d on port %d\n", i, si_other.sin_port);
     }
  }

}

int main(void)
{
    struct sockaddr_in si_me, si_other;

    int s, i, slen = sizeof(si_other) , recv_len;


    //create a UDP socket
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        die("socket");
    }

    // zero out the structure
    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket to port
    if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
    {
        die("bind");
    }
    for (i = 0; i<NUMBER_OF_SERVERS; i++){
      membership[i].active=0;
      membership[i].server_number=i;
      membership[i].timestamp = time(NULL);
    }
    membership[0].active=1;
    int server_number = 10;
    void * server_number_p = &server_number;
    pthread_t thread_k;
    pthread_create(&thread_k,NULL,keyboard_thread, &server_number_p);
    //keep listening for data
    while(1)
    {
        check_for_new(s);
    }

    close(s);
    return 0;
}
