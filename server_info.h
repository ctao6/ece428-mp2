#define NUMBER_OF_SERVERS 10

char *ip_address[] = {"172.22.158.38", "172.22.154.39",
	   		  "172.22.156.39", "172.22.158.39",
    			  "172.22.154.40", "172.22.156.40",
    			  "172.22.158.40", "172.22.154.41",
    			  "172.22.156.41", "172.22.158.41"};
char * intro_ip_address = "172.22.158.41";
struct Member_id {
	int active;
	int server_number;
	time_t timestamp;
};

void write_to_log(char * message)
{
	FILE *fp;

   	fp = fopen("vm.log", "w+");
   	fprintf(fp, "%s", message);
   	fclose(fp);
}
