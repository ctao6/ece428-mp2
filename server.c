/*
    Simple udp client
*/
#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include<pthread.h>
#include"server_info.h"
#define SERVER "127.0.0.1"
#define BUFLEN 512  //Max length of buffer
#define PORT 8888   //The port on which to send data
#define INTRODUCER_NUMBER 9
#define TIMEOUT 1000000
void die(char *s)
{
    perror(s);
    exit(1);
}


struct Member_id membership[NUMBER_OF_SERVERS];
void *keyboard_thread(void *vargp)
{
    while(1){
      char c = getchar();
      printf("serverid:%d\n",*((int *)vargp));
      int i;
      if(c == 'l')
      {
		for(i = 0; i < 10; i++)
		{
			if (membership[i].active==1)
				printf("Server %d Active flag %d, timestamp %lu\n", membership[i].server_number, membership[i].active, membership[i].timestamp);
		}
      }
    }
    return NULL;
}

struct sockaddr_in s2[NUMBER_OF_SERVERS];
int sock[10];

void setup_sockets()
{
	int i;
	for(i = 0; i < NUMBER_OF_SERVERS; i++)
	{

		if ( (sock[i]=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    		{
        		die("socket");
    		}

    		memset((char *) &s2[i], 0, sizeof(s2[i]));
    		s2[i].sin_family = AF_INET;
    		s2[i].sin_port = htons(PORT+i);
    		s2[i].sin_addr.s_addr = INADDR_ANY;
		struct timeval read_timeout;
		read_timeout.tv_sec = 0;
		read_timeout.tv_usec = 10;
		setsockopt(sock[i], SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
		int enable = 1;
		if (setsockopt(sock[i], SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    			error("setsockopt(SO_REUSEADDR) failed");
    		if ( bind(sock[i], (const struct sockaddr *)&s2[i],
            	sizeof(s2[i])) < 0 )
    		{
        		perror("bind failed");
        		exit(EXIT_FAILURE);
    		}
	}
}


//returns the server number for this server
int ask_introducer(struct sockaddr_in si_other, int s, int slen)
{
    char message[BUFLEN];

    if (inet_aton(intro_ip_address , &si_other.sin_addr) == 0)
    {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }
    message[0] = 'J';
    int server_number;
    if (sendto(s, message, strlen(message) , 0 , (struct sockaddr *) &si_other, slen)==-1)
    {
	die("sendto()");
    }
    //receive a reply and print it
    //clear the buffer by filling null, it might have previously received data
    //memset(buf,'\0', BUFLEN);
    //try to receive some data, this is a blocking call
    if (recvfrom(s, &server_number, 4, 0, (struct sockaddr *) &si_other, &slen) == -1)
    {
	die("recvfrom()");
    }
    printf("recived authorization from introducer joining as Server %d\n", server_number);
    //puts(buf);
    return server_number;
}

void ping(int this_server_number)
{
    int i, current_check;
    for(i = 0; i < 4; i++)
    {
	if(this_server_number+i+1 > 9)
		current_check = this_server_number + i - NUMBER_OF_SERVERS+2;
	else
		current_check = this_server_number+i+1;
    	if (inet_aton(ip_address[current_check-1] , &s2[current_check].sin_addr) == 0)
    	{
        	fprintf(stderr, "inet_aton() failed\n");
        	exit(1);
    	}
    	if (sendto(sock[current_check], &this_server_number, 4 , 0 , (struct sockaddr *) &s2[current_check], sizeof(s2[current_check]))==-1)
    	{
		die("sendto()");
    	}
    	//printf("Pinging Server %d on port %d\n", current_check, s2[current_check].sin_port);
   }


   	//si_other.sin_port = htons(PORT+this_server_number+1);
}

void send_ack(int dst_server_number, int this_server_number)
{
	if (inet_aton(ip_address[dst_server_number-1] , &s2[dst_server_number].sin_addr) == 0)
    	{
        	fprintf(stderr, "inet_aton() failed\n");
        	exit(1);
    	}
	int slen = sizeof(s2[dst_server_number]);
        if (sendto(sock[this_server_number], &this_server_number, 4 , 0 , (struct sockaddr *) &s2[this_server_number], slen)==-1)
        {
		die("sendto()");
    	}
    	//printf("Sending ack to Server %d on port %d\n", dst_server_number, s2[dst_server_number].sin_port);
}

void check_ack(int this_server_number, struct sockaddr_in si_other, int s)
{
    int i, current_check, server_number;
    int slen = sizeof(s2[this_server_number]);
    for(i = 0; i < 4; i++)
    {
	if(this_server_number+i+1 > 9)
		current_check = this_server_number + i - NUMBER_OF_SERVERS+2;
	else
		current_check = this_server_number+i+1;
    	if (recvfrom(sock[current_check], &server_number, 4, 0, (struct sockaddr *) &s2[current_check], &slen) == -1)
    	{
	/*		if (membership[current_check].active==0){

			}
			else{*/

			
			char to_send = (char) (current_check + '0');
			printf("Server %d has failed\n", current_check);
			if (inet_aton(intro_ip_address , &si_other.sin_addr) == 0)
				{
					fprintf(stderr, "inet_aton() failed\n");
					exit(1);
				}
			if (sendto(s, &to_send, 1 , 0 , (struct sockaddr *) &si_other, slen)==-1)
				{
				die("sendto()");
				}
			char message [255];
			char number[2];
			sprintf(message, "Server %d has failed\n", current_check);
			write_to_log(message);
			printf("Sent failure message to introducer %c\n", to_send);	
			//}
    	}
	else {
		//printf("ack from Server %d\n", server_number);
		send_ack(server_number, this_server_number);
	}
   }
}

void listen_ping(int this_server_number)
{
	int i, current_check;
	int server_number;
  int slen = sizeof(s2[this_server_number]);
	for(i = 0; i < 4; i++)
	{
 		if (recvfrom(sock[this_server_number], &server_number, 4, 0, (struct sockaddr *) &s2[this_server_number], &slen) == -1)
    		{
			break;
    		}
		else {
			if(server_number <= 9 && server_number != this_server_number)
			{
				//printf("ping from Server %d\n", server_number);
				send_ack(server_number, this_server_number);
				server_number = 0;
			}
		}
	}
}

void update_membership_list(struct sockaddr_in si_other, int s, int slen, int server_number)
{
    if (recvfrom(s, &membership, sizeof(struct Member_id)*10, 0, (struct sockaddr *) &si_other, &slen) == -1)
    {
	die("recvfrom()");
    }
    printf("updated membership list\n");
}
int main(void)
{
  struct sockaddr_in si_other;
	int s;
	if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    	{
        	die("socket");
    	}

    	memset((char *) &si_other, 0, sizeof(si_other));
    	si_other.sin_family = AF_INET;
    	si_other.sin_port = htons(PORT);
    	si_other.sin_addr.s_addr = INADDR_ANY;
/*    	if ( bind(s, (const struct sockaddr *)&si_other,
            sizeof(si_other)) < 0 )
    	{
        	perror("bind failed");
        	exit(EXIT_FAILURE);
    	}*/
    //setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
    setup_sockets();
    int server_number = ask_introducer(si_other, s, sizeof(si_other));
    void * server_number_p = &server_number;
    pthread_t thread_k;
    pthread_create(&thread_k,NULL,keyboard_thread,server_number_p);
//    pthread_join(thread_k,NULL);
    //setup_sockets(server_number, si_other, s, sizeof(si_other));
    while(1)
{
    ping(server_number);
    //send_ack(server_number, si_other, s, sizeof(si_other));
    listen_ping(server_number);
    usleep(TIMEOUT);
    check_ack(server_number, si_other, s);
    update_membership_list(si_other, s, sizeof(si_other), server_number);
}
//    listen_ping(server_number, si_other, s, sizeof(si_other));
/*
    while(1)
{
    ping(server_number, si_other, s, slen);
    send_ack(server_number, si_other, s, slen);
    wait_ack(server_number, si_other, s, slen);
}
*/
    //close(s);
    return 0;
}
